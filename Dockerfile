FROM python:3

WORKDIR /app

COPY . .

RUN python3 -m pip install pipenv

RUN pipenv install --system --deploy

