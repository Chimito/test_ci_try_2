import datetime

from django.contrib.auth.models import User
from rest_framework import authentication
from rest_framework.exceptions import AuthenticationFailed
from rest_framework.exceptions import AuthenticationFailed
from api.jwt_utils import decode_token


class GetUser:
    @classmethod
    def get_user(cls, username, password):
        user: User = User.objects.filter(username=username).first()
        if user is None:
            raise AuthenticationFailed(detail='User not found')
        if not user.check_password(password):
            raise AuthenticationFailed(detail='Invalid password')

        return user


class Authentication(authentication.BaseAuthentication):
    def authenticate(self, request):
        token = request.headers.get('Authorization')
        if not token:
            return None

        user_data = decode_token(token)
        username = user_data.get('username')
        expired = user_data.get('expired')
        user = User.objects.filter(username=username).first()

        if user is None:
            raise AuthenticationFailed('No such user')
        if expired < str(datetime.datetime.now()):
            raise AuthenticationFailed('Token has expired')

        return user, None


def update_token_expired(token: str):
    user_data = decode_token(token)
    expired = user_data.get('expired')
    new_expired = datetime.datetime