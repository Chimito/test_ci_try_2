import datetime

import jwt
from django.conf import settings
from django.contrib.auth.models import User


def encode_token(user: User):
    expired_time = datetime.datetime.now() + datetime.timedelta(minutes=settings.JWT_EXPIRED_INTERVAL_IN_MINUTES)
    return jwt.encode({'username': user.username,
                       'expired': str(expired_time)},
                      key=settings.SECRET_KEY,
                      algorithm='HS256')


def decode_token(token: str):
    return jwt.decode(token, key=settings.SECRET_KEY, algorithms='HS256')
