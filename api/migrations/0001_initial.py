# Generated by Django 4.0.5 on 2022-06-04 11:35

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Dog',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50)),
                ('weight', models.FloatField(default=0, null=True)),
                ('color', models.CharField(choices=[('1', 'Black'), ('2', 'Orange'), ('3', 'White')], max_length=30)),
            ],
        ),
    ]
