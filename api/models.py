from django.db import models
from django.contrib.auth.models import AbstractUser


class Dog(models.Model):
    class Colors(models.TextChoices):
        black = 1
        orange = 2
        white = 3

    name = models.CharField(max_length=50)
    weight = models.FloatField(default=0, null=True)
    color = models.CharField(choices=Colors.choices, max_length=30)
