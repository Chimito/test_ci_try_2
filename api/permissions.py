from django.contrib.auth.models import User
from rest_framework import permissions
from rest_framework.request import Request


class CreateDogPermission(permissions.BasePermission):
    """
    Global permission check for blocked IPs.
    """

    def has_permission(self, request: Request, view):
        if request.method in permissions.SAFE_METHODS:
            return True

        else:
            user: User = request.user
            return user.is_authenticated