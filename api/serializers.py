from django.contrib.auth.models import User
from rest_framework.serializers import ModelSerializer, Serializer, CharField, JSONField
from api.models import Dog


class DogSerializer(ModelSerializer):
    class Meta:
        model = Dog
        fields = ['color', 'name', 'weight']


class UserSerializer(ModelSerializer):
    class Meta:
        model = User
        fields = ['username', 'password', 'user_permissions']

    def create(self, validated_data):
        return User.objects.create_user(**validated_data)


class ObtainTokenSerializer(Serializer):
    token = CharField(max_length=250)
    description = "access_token"


class RefreshTokenSerializer(Serializer):
    token = CharField(max_length=250)


class UserLoginSerializer(Serializer):
    username = CharField(max_length=100)
    password = CharField(max_length=250)
