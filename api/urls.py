from django.db import router
from django.urls import path, include
from rest_framework.routers import DefaultRouter

from api.views import IndexView, DogList, DogDetail, UserViewSet, ObtainTokenView

router = DefaultRouter()
router.register(r'users', UserViewSet, basename="users")

urlpatterns = [
    path('', IndexView.as_view(), name='index'),
    path('dogs/', DogList.as_view()),
    path('dogs/<int:pk>/', DogDetail.as_view()),
    path('login/', ObtainTokenView.as_view()),
    path('', include(router.urls))
]
