import time

from django.contrib.auth.models import User
from rest_framework.permissions import IsAuthenticated
from rest_framework.request import Request
from rest_framework.viewsets import ModelViewSet
from rest_framework.decorators import throttle_classes, permission_classes
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.throttling import UserRateThrottle
from rest_framework.generics import GenericAPIView
from rest_framework import mixins
from rest_framework import status
from logging import Logger
from api.authentication import GetUser, Authentication
from api.jwt_utils import encode_token
from api.models import Dog
from api.permissions import CreateDogPermission
from api.serializers import DogSerializer, UserSerializer, UserLoginSerializer, ObtainTokenSerializer, \
    RefreshTokenSerializer


class OncePerMinuteThrottle(UserRateThrottle):
    rate = '1/minute'


class FivePerMinuteThrottle(UserRateThrottle):
    rate = '5/minute'


class IndexView(APIView):
    # authentication_classes = [Authentication, ]
    # permission_classes = [IsAuthenticated]

    def get(self, request: Request):
        return Response("ABOBA!")


class DogList(GenericAPIView,
              mixins.ListModelMixin,
              mixins.CreateModelMixin):
    queryset = Dog.objects.all()
    serializer_class = DogSerializer
    authentication_classes = [Authentication]
    permission_classes = [CreateDogPermission]

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)


class DogDetail(GenericAPIView,
                mixins.RetrieveModelMixin):
    queryset = Dog.objects.all()
    serializer_class = DogSerializer
    permission_classes = [CreateDogPermission]

    def get(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)


class UserViewSet(ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer


class ObtainTokenView(APIView):
    def post(self, request):
        serializer = UserLoginSerializer(data=request.data)
        if not serializer.is_valid():
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        user = GetUser.get_user(**serializer.data)
        if user is None:
            return Response("User not found", status=status.HTTP_400_BAD_REQUEST)

        token = encode_token(user)
        return Response({'type': 'access_token', 'token': token}, status=status.HTTP_200_OK)


class RefreshTokenView(APIView):
    authentication_classes = [Authentication]
    permission_classes = [IsAuthenticated]

    def post(self, request: Request):
        serializer = RefreshTokenSerializer(data=request.data)
        if not serializer.is_valid():
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        token = serializer.data.get('token')
